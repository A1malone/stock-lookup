# Stock Lookup

Stock Lookup is a stock market web app used to research financial markets. Find real time quotes and news. As well as track Forex, equities, ETFs, indices, options, cryptos, and commodities.

## Installation

```bash
npm i
```

## use to run create a production build

```bash
npm run build
```

#### use to test

```bash
npm run start
```

## REPLACE TOKEN WITH YOUR OWN GENERATED FROM IEX API

sign up [HERE](https://iexcloud.io/)

#### replace in config as follows:

```bash
TEST_SECRET_KEY=sk_(some generated number)
```

### PLEASE PAY ATTENTION TO SANDBOX VIEW!

in Sandbox View the

```bash
sk_
```

part of the token changes.

## Contributing

Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

Please make sure to update tests as appropriate.

## License

[MIT](https://choosealicense.com/licenses/mit/)
