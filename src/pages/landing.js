import React from "react";
import Searchbar from ".././components/search/search";
import { Prompt } from "react-router-dom";

const Landing = (props) => {
  return (
    <>
      <div className="landingBG">
        <Prompt when={props.isValid} message={`please put input a ticker`} />
        <main className="landing">
          <section>
            <h1 className="landing-title">STOCK LOOKUP</h1>
            <div className="d-flex flex-column">
              <Searchbar getQuote={props.getQuote} />
            </div>
          </section>
        </main>
      </div>
    </>
  );
};

export default Landing;
