import React from "react";
import FinaleQuote from "../components/quote/finalQuote";
import Spinner from "../components/new/spinner";
import Navbar from "../components/navigation/navbar";

const Quote = (props) => {
  return (
    <>
      <Navbar getQuote={props.getQuote} />
      {props.isLoaded ? (
        <FinaleQuote
          //chart state
          lineChange={props.lineChange}
          handleChartTime={props.handleChartTime}
          isLoaded={props.isLoaded}
          color={props.pctCh}
          name={props.name}
          symbol={props.symbol}
          price={props.price}
          pctCh={props.pctCh}
          pct={props.pct}
          close={props.close}
          label={props.label}
          chart={props.chart}
          //profile state
          chartNews={props.chartNews}
          open={props.open}
          prevClose={props.prevClose}
          EPS={props.EPS}
          wkH={props.wkH}
          wkL={props.wkL}
          mktCa={props.mktCa}
          divY={props.divY}
          beta={props.beta}
          volume={props.volume}
          peRatio={props.peRatio}
          description={props.description}
          ceo={props.ceo}
          sector={props.sector}
          exchange={props.exchange}
          website={props.website}
          employees={props.employees}
        />
      ) : (
        <Spinner />
      )}
    </>
  );
};

export default Quote;
