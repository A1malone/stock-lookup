import React, { Component } from "react";
import Landing from "./pages/landing";
import Quote from "./pages/finalQuote";
import { Switch, Route } from "react-router-dom";
import axios from "axios";

class Routes extends Component {
  constructor(props) {
    super(props);
    this.handleChartTime = this.handleChartTime.bind(this);
    this.getBatch = this.getBatch.bind(this);

    this.state = {
      chart: [],
      symbol: "",
      chartTime: "",
      lineChange: false,
      news: [],
      quote: [],
      company: [],
      isLoaded: false,
      isValid: true,
    };
  }

  handleChartTime(range) {
    this.setState({
      lineChange: false,
    });
    axios
      .get(
        `https://sandbox.iexapis.com/stable/stock/${this.state.symbol}/chart/${range}?token=Tsk_c3f1c09530a611e9958142010a80043c`
      )
      .then((res) => {
        let chart = res.data;
        console.log(`chart range (${range}): ${chart} `);
        this.setState({
          chart,
          lineChange: true,
        });
        //console.log({ chart, news, quote });
        //return { chart, news, quote };
      })
      .catch((err) => {
        if (err.response) {
          //request made server responded with status code
          console.log(err.response);
          alert(`RESPONSE ERROR: status code ${err.response.status}`);
        } else if (err.request) {
          //request was made but server did not respond
          console.log(err.request);
          alert("REQUEST ERROR: request was made but server did not respond");
        } else {
          //something happened in the request that triggered the error
          console.log(`Error: ${err.message}`);
          alert("SEARCH ERROR PLEASE TRY AGAIN OR REPORT THIS ERROR");
        }
        console.log(err.config);
      });
  }

  //use for full quote search
  getBatch = async (symbol) => {
    this.setState({
      symbol,
    });
    await axios
      .get(
        `https://sandbox.iexapis.com/stable/stock/${symbol}/batch?types=quote,news,chart&range=1m&last=30&token=Tsk_c3f1c09530a611e9958142010a80043c`
      )
      .then((res) => {
        let { chart, news, quote } = res.data;
        this.setState({
          chart,
          news,
          quote,
          isLoaded: true,
          lineChange: true,
        });
        //console.log({ chart, news, quote });
        //return { chart, news, quote };
      })
      .catch((err) => {
        if (err.response) {
          //request made server responded with status code
          console.log(err.response);
          if (err.response.data === "Not Found") {
            return this.setState({ isValid: false });
            //return this.setState({ isValid: false });
          } else {
            return alert(
              `RESPONSE ERROR: status code ${err.response.status} \n MSG: ${err.response.data}`
            );
          }
        } else if (err.request) {
          //request was made but server did not respond
          console.log(err.request);
          return alert(
            "REQUEST ERROR: request was made but server did not respond"
          );
        } else {
          //something happened in the request that triggered the error
          console.log(`Error: ${err.message}`);
          return alert("SEARCH ERROR PLEASE TRY AGAIN OR REPORT THIS ERROR");
        }
        console.log(err.config);
      });

    await axios
      .get(
        `https://sandbox.iexapis.com/stable/stock/${symbol}/company?token=Tsk_c3f1c09530a611e9958142010a80043c`
      )
      .then((res) => {
        console.log(res);
        let company = res.data;
        this.setState({
          company,
        });
        console.log(company);
      });
    /*.catch((err) => {
        if (err.response) {
          //request made server responded with status code
          console.log(err.response);
          return alert(`RESPONSE ERROR: status code ${err.response.status}`);
        } else if (err.request) {
          //request was made but server did not respond
          console.log(err.request);
          return alert(
            "REQUEST ERROR: request was made but server did not respond"
          );
        } else {
          //something happened in the request that triggered the error
          console.log(`Error: ${err.message}`);
          return alert("SEARCH ERROR PLEASE TRY AGAIN OR REPORT THIS ERROR");
        }
        console.log(err.config);
      });*/
  };
  /*
  componentDidMount() {
    axios
      .get(
        `https://sandbox.iexapis.com/stable/stock/${"aapl"}/batch?types=quote,news,chart&range=1m&last=10&token=Tsk_c3f1c09530a611e9958142010a80043c`
      )
      .then((res) => {
        let { chart, news, quote } = res.data;
        this.setState({
          chart,
          news,
          quote,
          isLoaded: true,
          lineChange: true,
        });
        //console.log({ chart, news, quote });
        //return { chart, news, quote };
      })
      .catch((err) => {
        if (err.response) {
          //request made server responded with status code
          console.log(err.response);
          alert(`RESPONSE ERROR: status code ${err.response.status}`);
        } else if (err.request) {
          //request was made but server did not respond
          console.log(err.request);
          alert("REQUEST ERROR: request was made but server did not respond");
        } else {
          //something happened in the request that triggered the error
          console.log(`Error: ${err.message}`);
          alert("SEARCH ERROR PLEASE TRY AGAIN OR REPORT THIS ERROR");
        }
        console.log(err.config);
      });

    axios
      .get(
        `https://sandbox.iexapis.com/stable/stock/${"aapl"}/company?token=Tsk_c3f1c09530a611e9958142010a80043c`
      )
      .then((res) => {
        console.log(res);
        let company = res.data;
        this.setState({
          company,
        });
        console.log(company);
      })
      .catch((err) => {
        if (err.response) {
          //request made server responded with status code
          console.log(err.response);
          alert(`RESPONSE ERROR: status code ${err.response.status}`);
        } else if (err.request) {
          //request was made but server did not respond
          console.log(err.request);
          alert("REQUEST ERROR: request was made but server did not respond");
        } else {
          //something happened in the request that triggered the error
          console.log(`Error: ${err.message}`);
          alert("SEARCH ERROR PLEASE TRY AGAIN OR REPORT THIS ERROR");
        }
        console.log(err.config);
      });
  } */
  retrycall = (callType, apiLink) => {
    setTimeout(function () {
      fetch(apiLink)
        .then((response) => response.json())
        .then();
    }, 1000);
  };

  render() {
    const { quote, chart, news, company } = this.state;
    return (
      <>
        <Switch>
          <Route
            exact
            path="/"
            render={() => (
              <Landing
                isValid={this.state.isValid}
                getQuote={this.getBatch.bind(this)}
              />
            )}
          />
          <Route
            exact
            path="/quote"
            render={() => (
              <Quote
                //chart state
                getQuote={this.getBatch.bind(this)}
                handleChartTime={this.handleChartTime.bind(this)}
                lineChange={this.state.lineChange}
                isLoaded={this.state.isLoaded}
                color={quote.change}
                name={quote.companyName}
                symbol={quote.symbol}
                price={quote.latestPrice}
                pctCh={quote.change}
                pct={quote.changePercent}
                close={chart.map((close) => close.close)}
                label={chart.map((label) => label.label)}
                chart={chart}
                //profile state
                chartNews={news}
                open={quote.open}
                prevClose={quote.previousClose}
                EPS={this.state.EPS}
                wkH={quote.week52High}
                wkL={quote.week52Low}
                mktCa={quote.marketCap}
                divY={this.state.divY}
                beta={this.state.beta}
                volume={quote.volume}
                peRatio={quote.peRatio}
                description={company.description}
                ceo={company.CEO}
                sector={company.sector}
                exchange={company.exchange}
                website={company.website}
                employees={company.employees}
              />
            )}
          />
        </Switch>
      </>
    );
  }
}
export default Routes;
