import React from "react";
import { Line } from "react-chartjs-2";
import Spinner from "../new/spinner";

const lineColor = (pct) => {
  const value = pct < 0 ? "red" : "green";
  return value;
};

const lineChart = (props) => {
  let data = {
    labels: props.label,
    datasets: [
      {
        label: "price USD",
        fill: false,
        lineTension: 0.1,
        backgroundColor: lineColor(props.color),
        borderColor: lineColor(props.color),
        borderCapStyle: "butt",
        borderDash: [],
        borderDashOffset: 0.0,
        borderJoinStyle: "miter",
        pointBorderColor: lineColor(props.color),
        pointBackgroundColor: "#fff",
        pointBorderWidth: 1,
        pointHoverRadius: 5,
        pointHoverBackgroundColor: "rgba(75,192,192,1)",
        pointHoverBorderColor: "rgba(220,220,220,1)",
        pointHoverBorderWidth: 2,
        pointRadius: 1,
        pointHitRadius: 1,
        data: props.close,
      },
    ],
  };
  //console.log(props.label);
  //console.log(props.close.map(x => x.toString()));
  return (
    <>
      {props.lineChange ? (
        <Line
          data={data}
          options={{
            legend: { display: false },
            tooltips: { intersect: false },
          }}
        />
      ) : (
        <Spinner />
      )}
    </>
  );
};
export default lineChart;
