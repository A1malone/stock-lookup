import React from "react";
import Line from "./line";
import Nav from "react-bootstrap/Nav";
import Quotebox from "../quote/quotebox";
import Spinner from "../new/spinner";

const Chart = (props) => {
  return (
    <>
      <div className="Charts">
        <Quotebox
          name={props.name}
          symbol={props.symbol}
          price={props.price}
          pctCh={props.pctCh}
          pct={props.pct}
        />
        {props.isLoaded ? (
          <Line
            close={props.close}
            label={props.label}
            color={props.color}
            lineChange={props.lineChange}
          />
        ) : (
          <Spinner />
        )}
        {
          <Nav
            className="justify-content-center mt-3"
            variant="pills"
            defaultActiveKey="/home"
          >
            <Nav.Item>
              <Nav.Link
                onClick={() => props.handleChartTime("1m")}
                eventKey="link-1"
              >
                1M
              </Nav.Link>
            </Nav.Item>
            <Nav.Item>
              <Nav.Link
                onClick={() => props.handleChartTime("3m")}
                eventKey="link-2"
              >
                3M
              </Nav.Link>
            </Nav.Item>
            <Nav.Item>
              <Nav.Link
                onClick={() => props.handleChartTime("1y")}
                eventKey="link-3"
              >
                1Y
              </Nav.Link>
            </Nav.Item>
            <Nav.Item>
              <Nav.Link
                onClick={() => props.handleChartTime("ytd")}
                eventKey="link-4"
              >
                YTD
              </Nav.Link>
            </Nav.Item>
          </Nav>
        }

        <p className="attribution text-center">IEX Real-Time Price</p>
      </div>
    </>
  );
};

export default Chart;
