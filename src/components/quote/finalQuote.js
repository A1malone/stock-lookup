import React from "react";
import Chart from "../chart/chart";
import Profile from "../quote/company-profile";
import News from "../news/news";

const FinaleQuote = (props) => {
  return (
    <>
      <div className="container-fluid p-5">
        <div className="row">
          <div className="col-sm col-lg-7">
            <Chart
              lineChange={props.lineChange}
              handleChartTime={props.handleChartTime}
              isLoaded={props.isLoaded}
              color={props.color}
              name={props.name}
              symbol={props.symbol}
              price={props.price}
              pctCh={props.pctCh}
              pct={props.pct}
              //chartChange={this.handleChartChange}
              close={props.close}
              label={props.label}
            />
            <div className="col-sm">
              <News news={props.chartNews} />
            </div>
          </div>
          <div className="col-sm col-lg-5">
            <Profile
              className="col-sm"
              open={props.open}
              wkH={props.wkH}
              wkL={props.wkL}
              mktCap={props.mktCa}
              volume={props.volume}
              //beta does not work when .toFixed(2) is added
              beta={props.beta}
              divY={props.divY}
              peR={props.peRatio}
              close={props.prevClose}
              EPS={props.EPS}
              description={props.description}
              ceo={props.ceo}
              sector={props.sector}
              exchange={props.exchange}
              website={props.website}
              employees={props.employees}
            />
          </div>
        </div>
      </div>
    </>
  );
};

export default FinaleQuote;
