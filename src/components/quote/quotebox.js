import React from "react";

function getPctColor(value) {
  let colors = "change change-";
  colors += value < 0 ? "red" : "green";
  return colors;
}

const Quotebox = (props) => {
  return (
    <>
      <div className="row mb-3">
        <div className="col-8">
          <div className="row">
            <div className="col-12">
              <div className="quote-name">{props.name}</div>
              <div className="sym-size">
                (<span className="symbol">{props.symbol}</span>)
              </div>
              <div className="d-flex flex-row">
                <span className="quote-price">{props.price}</span>
                <span className={getPctColor(props.pctCh)}>
                  <span className="quote-change">{props.pctCh}</span>(
                  <span className="quote-pct">{props.pct}</span>
                  %)
                </span>
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
};
export default Quotebox;
