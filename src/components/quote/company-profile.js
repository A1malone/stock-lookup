import React from "react";
const numFormat = (value) => {
  const n = value.toString();
  let num = n;
  // changed to switch statement will check if works wheni use data
  switch (n.length) {
    case 12:
      num = n[0] + n[1] + n[2] + "." + n[3] + "B";
      return num;
    case 11:
      num = n[0] + n[1] + "." + n[2] + "B";
      return num;
    case 10:
      num = n[0] + "." + n[1] + "B";
      return num;
    case 9:
      num = n[0] + n[1] + n[2] + "." + n[3] + "M";
      return num;
    case 8:
      num = n[0] + n[1] + "." + n[2] + "M";
      return num;
    case 7:
      num = n[0] + "." + n[1] + "M";
      return num;
    case 15:
      num = n[0] + +n[1] + n[2] + "." + n[3] + "T";
      return num;

    case 14:
      num = n[0] + n[1] + "." + n[2] + "T";
      return num;
    case 13:
      num = n[0] + "." + n[1] + "T";
      return num;
    default:
      return num;
  }
};

const Profile = (props) => {
  return (
    <>
      <h1 id="section-title">
        <strong>Company Profile </strong>
      </h1>
      <hr />
      <div className="CpC row">
        {/*column 1*/}
        <div className="col-sm col-md-12 col-lg-6">
          <div className="d-flex flex-row">
            <div className="info-header">Open</div>
            <div className="info-item">
              <span className="open">{props.open}</span>
            </div>
          </div>
          <div className="d-flex flex-row">
            <div className="info-header">Market Cap</div>
            <div className="info-item">
              <span className="mktCap">{numFormat(props.mktCap)}</span>
            </div>
          </div>
          <div className="d-flex flex-row">
            <div className="info-header">Volume</div>
            <div className="info-item">
              <span className="volume">{numFormat(props.volume)}</span>
            </div>
          </div>
        </div>
        {/*column 2*/}
        <div className="col-sm col-md-12 col-lg-6">
          <div className="d-flex flex-row">
            <div className="info-header">Prev Close</div>
            <div className="info-item">
              <span className="stock-close">{props.close}</span>
            </div>
          </div>
          <div className="d-flex flex-row">
            <div className="info-header">52Wk Range</div>
            <div className="info-item">
              <span className="wkH">{props.wkH}</span> -{" "}
              <span className="wkL">{props.wkL}</span>
            </div>
          </div>
          <div className="d-flex flex-row">
            <div className="info-header">P/E Ratio</div>
            <div className="info-item">
              <span className="peR">{props.peR}</span>
            </div>
          </div>
        </div>
      </div>

      <div className="row text-center">
        <div className="col">
          <div className="info-header">CEO</div>
          <div className="info-item">
            <span className="ceo">{props.ceo}</span>
          </div>
        </div>
        <div className="col">
          <div className="info-header">Employees</div>
          <div className="info-item">
            <span className="ceo">{props.employees}</span>
          </div>
        </div>
        <div className="col">
          <div className="info-header">EXCHANGE</div>
          <div className="info-item">
            <span className="exchange">{props.exchange}</span>
          </div>
        </div>
        <div className="col">
          <div className="info-header">SECTOR</div>
          <div className="info-item">
            <div className="info-item">
              <span className="sec">{props.sector}</span>
            </div>
          </div>
        </div>
      </div>

      <div className="row">
        <div className="col-12">
          <div className="info-header">Description</div>
          <div className="info-item">
            <span className="divY">{props.description}</span>
          </div>
        </div>
        <div className="col">
          <div className="info-item">
            <a href={props.website} className="website">
              <button type="button" className="btn btn-success">
                Visit Website
              </button>
            </a>
          </div>
        </div>
      </div>
    </>
  );
};
export default Profile;
