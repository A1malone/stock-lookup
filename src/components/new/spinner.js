import React from "react";
import Spinner from "react-bootstrap/Spinner";

const Spin = () => {
  return (
    <div className="d-flex justify-content-center spinner-space">
      <Spinner animation="border" role="status">
        <span className="visually-hidden">Loading...</span>
      </Spinner>
    </div>
  );
};

export default Spin;
