import React from "react";

const News = (props) => {
  return (
    <>
      <h1 className="mt-4" id="section-title">
        <strong>News</strong>
      </h1>
      <hr />
      <div id="scrollable">
        <div className="row news-row">
          {props.news.map((data, i) => (
            <a href={data.url} key={data.key} className="col-md-12 col-lg-6">
              <div className="card mb-3 news-card">
                <div className="row no-gutters">
                  {/*<div class="col-md-4">
                      <img src={data.image} class="card-img" alt="News" />
                    </div> */}
                  <div className="col-md-12">
                    <div className="card-body">
                      <h5 className="card-title headline">
                        <strong>{data.headline}</strong>
                      </h5>
                      <p className="card-text summary">{data.summary}</p>
                      <p className="card-text">
                        <small className="text-muted">
                          Source: {data.source}
                        </small>
                      </p>
                    </div>
                  </div>
                </div>
              </div>
            </a>
          ))}
        </div>
      </div>
    </>
  );
};

export default News;
