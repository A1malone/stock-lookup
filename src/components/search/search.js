import React, { useState } from "react";
import { Link } from "react-router-dom";

const Search = (props) => {
  const [input, setInput] = useState("");
  return (
    <>
      <div
        className="mx-auto search"
        //onSubmit={props.getQuote}
        //onClick={props.getQuote}
      >
        <div className="input-group custom-button">
          <input
            type="text"
            className="form-control "
            placeholder="Search Ticker"
            aria-label="Searchbard"
            value={input}
            onChange={(e) => setInput(e.target.value)}
          />
          <div className="input-group-append">
            <Link
              to="/quote"
              onClick={() => props.getQuote(input)}
              className="btn btn-dark"
              id="button-addon2"
              type="submit"
            >
              Search
            </Link>
          </div>
        </div>
      </div>
    </>
  );
};

export default Search;
