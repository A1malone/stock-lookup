import React from "react";
import Navbar from "react-bootstrap/Navbar";
import Search from "../search/search";
import { Link } from "react-router-dom";

const Nav = (props) => {
  return (
    <>
      <Navbar className="navbar-dark bg-space">
        <div className="container">
          <div className="navbar-brand mb-0 h1 nav-brand ">
            <Link to="/" className="white-link">
              <strong>Stock Lookup</strong>
            </Link>
          </div>

          <Search getQuote={props.getQuote} />
        </div>
      </Navbar>
    </>
  );
};
export default Nav;
