import axios from "axios";

export const SearchAPI = (symbol) => {
  axios
    .get(
      `https://sandbox.iexapis.com/stable/stock/${symbol}/batch?types=quote,news,chart&range=1m&last=10&token=Tsk_c3f1c09530a611e9958142010a80043c`
    )
    .then((res) => {
      let { chart, news, quote } = res.data;
      console.log({ chart, news, quote });
      return { chart, news, quote };
    })
    .catch((err) => {
      if (err.response) {
        //request made server responded with status code
        console.log(err.response);
        alert(`RESPONSE ERROR: status code ${error.response.status}`);
      } else if (err.request) {
        //request was made but server did not respond
        console.log(err.request);
        alert("REQUEST ERROR: request was made but server did not respond");
      } else {
        //something happened in the request that triggered the error
        console.log(`Error: ${err.message}`);
        alert("SEARCH ERROR PLEASE TRY AGAIN OR REPORT THIS ERROR");
      }
      console.log(err.config);
    });
};
